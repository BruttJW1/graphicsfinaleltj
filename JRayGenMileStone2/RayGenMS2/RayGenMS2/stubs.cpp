/**
  This testing framework has been developed/overhauled over time, primarily by:
  Chris Czyzewicz
  Ben Sunshine-Hill
  Cory Boatright 
  
  While the three were PhD students at UPenn (through 2013).  This particular version has some
  modifications by Cory since joining the faculty of Grove City College.
  
  Last revised 4/10/2017
*/

#include "stubs.h"

// Tries to find the intersection of a ray and a sphere.
// P0 is the position from which the ray emanates; V0 is the
// direction of the ray.
// matrix is the transformation matrix of the sphere.
// This function should return the smallest positive t-value of the intersection
// (a point such that P0+t*V0 intersects the sphere), or -1 if there is no
// intersection.
double Test_RaySphereIntersect(const gVector4& P0, const gVector4& V0, const gMatrix4& T) {

	int r = 1;
	float t1 = 0;
	float t2 = 0;
	gMatrix4 T1 = T.transpose().invert(); //required by Boatright

	//P1 = P * C^-1 (where C is the inverse function on T   and   P is the "starting point")
	gVector4 P = T1 * P0;
	gVector4 V = T1 * V0;

	//P = P1 + tV

	gVector4 O = gVector4(0.0f,0.0f,0.0f,1.0f);

	//gVector4 P1 = P0 + t*V0; //P1 = P0 + tV w/ sphere |P1-O|^2 -r^2 = 0   //What is the O?
	//substitute ray form into sphere equation |P1 + tV - O|^2 -r^2 = 0

	//Solve quadratic equation at^2 + bt + c = 0 where      #note that O is labeled on the graph as the center

	float a = V.length() * V.length(); //a = V * V
	float b = (2 * V) * (P - O); //b = 2V * (P0 - O)
	float c = ((P - O).length() * (P - O).length()) - (r * r); //c = |P0 - O|^2 - r^2 

	if ((2 * a) <= 0){ //if the denominator is less than 0, return -1
		return -1;
	}

	// t (-b +- squareroot(b^2 - 4ac)/2a
	t1 = (-b + sqrt(b * b - 4 * a * c)) / (2 * a);
	t2 = (-b - sqrt(b * b - 4 * a * c)) / (2 * a);

	if((t1 < 0) && (t2 < 0)){ //the direction is fine, but it's behind you.  No intersection.
		return -1;
	} else if(t1 < t2){
		return t1;
	} else {
		return t2;
	}

	//return -1;
}

// Tries to find the intersection of a ray and a triangle.
// This is just like the above function, but it intersects the ray with a
// triangle instead. The parameters p1, p2, and p3 specify the three
// points of the triangle, in object space.
double Test_RayPolyIntersect(const gVector4& P0, const gVector4& V0, const gVector4& p1, const gVector4& p2, const gVector4& p3, const gMatrix4& T) {
	gMatrix4 T1 = T.transpose().invert();  //Transpose and then invert

	gVector4 P = T1 * P0; //Convert to the object space
	gVector4 V = T1 * V0;

	gVector4 n = ((p1 - p2) % (p1 - p3)).getNormalized(); //normalize the cross product

	float t = (n * (p1 - P)) / (n * V);  

	if(t < 0){ //if t is negative, return -1
		return -1;
	}

	gVector4 r = P + V*t;

	gVector4 r1 = r - p1; //find the different values of r
	gVector4 r2 = r - p2;
	gVector4 r3 = r - p3;
	gVector4 p12 = p1 - p2; //make the sides
	gVector4 p13 = p1 - p3;
	
	float s = (p12 % p13).length(); //find the areas
	float s1 = (r1 % r2).length();
	float s2 = (r2 % r3).length();
	float s3 = (r3 % r1).length();

	float st = s1 + s2 + s3; //combine the areas

	if(abs(s-st) > 0.0001){
		return -1;
	}

	return t;
}

// This is just like Test_RaySphereIntersect, but with a unit cube instead of a
// sphere. A unit cube extends from -0.5 to 0.5 in all axes.
double Test_RayCubeIntersect(const gVector4& P0, const gVector4& V0, const gMatrix4& T) {
	float far  = 1e26; //tengazillion must be big enough (should probably be infinite)
	float near = -1e26;
	
	gMatrix4 T1 = T.transpose().invert(); //transpose and invert

	gVector4 e = T1 * P0; //find e and p
	gVector4 p = (T1 * P0 + T1 * V0);

	float t1;
	float t2;

	t1 = (-0.5 - e[0])/(p[0] - e[0]); //find the x plane
	t2 = (0.5 - e[0])/(p[0] - e[0]);

	if(t1 > t2){ //swap if t1 is greater than t2
		float temp = t2;
		t2 = t1;
		t1 = temp;
	}
	
	if(t1 > near){ //check the locations of t1 and t2
		near = t1;
	}
	if(t2 < far){
		far = t2;
	}

	t1 = (-0.5 - e[1])/(p[1] - e[1]); //find the y plane
	t2 = (0.5 - e[1])/(p[1] - e[1]);

	if(t1 > t2){ //swap if t1 is greater than t2
		float temp = t2;
		t2 = t1;
		t1 = temp;
	}
	
	if(t1 > near){
		near = t1;
	}
	if(t2 < far){
		far = t2;
	}

	t1 = (-0.5 - e[2])/(p[2] - e[2]); //find the z plane
	t2 = (0.5 - e[2])/(p[2] - e[2]);

	if(t1 > t2){ //check if t1 is greater than t2
		float temp = t2;
		t2 = t1;
		t1 = temp;
	}
	
	if(t1 > near){ //check the locations of t1 and t2
		near = t1;
	}
	if(t2 < far){
		far = t2;
	}

	if(far < near){ //if the far is less than near then it didn't hit anything
		return -1;
	}else{
		return near;
	}
}