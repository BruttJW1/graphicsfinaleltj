/*
  File:        RayGenMS2.cpp for Mile2RayGeneration
  Author:      Jonathan Brutt
  Class:	   Comp 361 A
  Purpose:     The purpose of this program is to create a ray for a BMP.
*/

#include <fstream>
#include <iostream>
#include <math.h> 
#include "RayGeneration.h"
#include "gVector4.h"
#include "gMatrix4.h"
#include "stubs.h"

using std::ifstream;
using std::cout;
using std::string;
using std::endl;
#include <string>
using std::ofstream;

RayGeneration::RayGeneration(){
};

RayGeneration::~RayGeneration(void){
};

void RayGeneration::parse(const std::string& filename) {
	
	ifstream fin;
	fin.open(filename);
	if (fin.fail())  //Checks if the file was opened successfully
	{
    cout << "Failed to open" << filename << " for input" << endl;
    exit(1);
	}

	string temp = ""; //Used to eliminate the 4 character pieces
	fin >> temp;
	string name;
	fin >> name; //name

	unsigned int width;
	unsigned int height;
	fin >> temp; //RESO
	fin >> width;
	fin >> height;

	float eyepx, eyepy, eyepz;
	fin >> temp; //EYEP
	fin >> eyepx;
	fin >> eyepy;
	fin >> eyepz;

	float vdirx, vdiry, vdirz;
	fin >> temp; //VDIR
	fin >> vdirx;
	fin >> vdiry;
	fin >> vdirz;

	float uvecx, uvecy, uvecz;
	fin >> temp; //UVEC
	fin >> uvecx;
	fin >> uvecy;
	fin >> uvecz;

	float fovy;
	fin >> temp; //FOVY
	fin >> fovy;

	fin >> temp >> temp >> temp >> temp;

	int shape;
	fin >> temp;
	fin >> shape;
	
	int transform;
	fin >> temp;
	fin >> transform;

	fin.close();
	//Starts BMP Writing

	vdirx = 0-eyepx;
	vdiry = 0-eyepy;
	vdirz = 0-eyepz;

	vdirx = (vdirx / (sqrt(pow(vdirx, 2.0) + pow(vdiry, 2.0) + pow(vdirz, 2.0))));
	vdiry = (vdiry / (sqrt(pow(vdirx, 2.0) + pow(vdiry, 2.0) + pow(vdirz, 2.0))));
	vdirz = (vdirz / (sqrt(pow(vdirx, 2.0) + pow(vdiry, 2.0) + pow(vdirz, 2.0))));

	//vector v = up vector tan phi
	//vector H = u vector tan theta
	// n vector
	//view direction - eye direction = n vector
	// vdir - eyep = n vector
	// at.x - eye.x
	// at.y - eye.y
	// at.z - eye.z
	//M = eye + n vector
	// n vector cross up
	//D vector = M + (2 * x/(width-1)-1 * H vector + (2 * y/(height-1)-1)* V vector
	float Dx;
	float Dy;
	float Dz;
	//ray.direction vector = D vector - Eye vector

	//width = 500;
	//height = 500;

	fovy = fovy * (3.1415926/180.0);
	//float theta = fovy/2 * ((float)width/(float)height); 
	float theta = fovy; 
	float tanphi = tan(theta / ((float)width/(float)height)); 

	BMP output;
	output.SetSize(width, height);
	output.SetBitDepth(24);

	//ofstream fout(name + ".txt");

	for(unsigned int x = 0; x < width; x++){
		for(unsigned int y = 0; y < height; y++){
	//{{
	//	unsigned int x = width/2;
	//	unsigned int y = height/2;
			//EYEP is the location of the eye
			gVector4 eye(eyepx, eyepy, eyepz, 0.0f);
			//VDIR is the view direction (NOT THE V VECTOR)
			//UVEC is the up vector (NOT THE U VECTOR)
			//FOVY is phi
			//Calc N for M, u for H, V.

			//n vector = vdir - eyep
			gVector4 n(vdirx, vdiry, vdirz, 0.0f);

			//M = eye + n vector
			gVector4 M(eyepx + n[0],eyepy + n[1],eyepz + n[2], 0.0f);

			//u = u1 + u2 + u3
			//u = nx + ny + nz
			//v = v1 + v2 + v3
			//v = uvecx + uvecy + uvecz
			//u x v = n x up
			//ux s1 = u2v3 - u3v2
			//uy s2 = u3v1 - u1v3
			//uz s3 = u1v2 - u2v1

			// n vector cross up
			gVector4 u(n[1]*uvecz - n[2]*uvecy, n[2]*uvecx - n[0]*uvecz, n[0]*uvecy - n[1]*uvecx, 0.0f);

			//vector H = u vector tan theta
			gVector4 H(u[0] * tan(theta), u[1] * tan(theta), u[2] * tan(theta), 0.0f);

			//vector V = up vector tan phi
			gVector4 v(uvecx * tanphi, uvecy * tanphi, uvecz * tanphi, 0.0f);

			//D vector = M + (2 * x/(width-1)-1 * H vector + (2 * y/(height-1)-1)* V vector
			Dx = M[0] + (2 * (float)x/((float)width-1)-1) * H[0] + (2 * (float)y/((float)height-1)-1) * v[0];
			Dy = M[1] + (2 * (float)x/((float)width-1)-1) * H[1] + (2 * (float)y/((float)height-1)-1) * v[1];
			Dz = M[2] + (2 * (float)x/((float)width-1)-1) * H[2] + (2 * (float)y/((float)height-1)-1) * v[2];
			gVector4 D(Dx, Dy, Dz, 0.0f);

			//ray.direction vector = D vector - Eye vector
			gVector4 rayD(D[0] - eyepx, D[1] - eyepy, D[2] - eyepz, 0.0f);

			//Normalizing x/ sqrt(x^2 + y^2 + z^2)
			float rayDx = rayD[0];
			float rayDy = rayD[1];
			float rayDz = rayD[2];
			rayD[0] = (rayDx / (sqrt(pow(rayDx, 2.0) + pow(rayDy, 2.0) + pow(rayDz, 2.0))));
			rayD[1] = (rayDy / (sqrt(pow(rayDx, 2.0) + pow(rayDy, 2.0) + pow(rayDz, 2.0))));
			rayD[2] = (rayDz / (sqrt(pow(rayDx, 2.0) + pow(rayDy, 2.0) + pow(rayDz, 2.0))));

			float t;
			gMatrix4 transMat = gMatrix4::scale3D(1.5f,1,1) * gMatrix4::rotateY(45) * gMatrix4::translate3D(0,1,0); //follow instructions about scaling, rotation, translation
			gMatrix4 transformMat = transform == 1 ? transMat : gMatrix4(); 
			gVector4 normal;
			gVector4 point;
			gVector4 objCent;
			bool foundEdge = false;
			
			switch(shape){ //a switch to choose the correct shape type
			case 1:

				t = Test_RaySphereIntersect(gVector4(eyepx, eyepy, eyepz, 1), rayD, transformMat);
				point = gVector4(eyepx, eyepy, eyepz, 1) + rayD * t;
				objCent = transformMat * gVector4(0,0,0,1);
				normal = (point - objCent).getNormalized();
				break;

			case 2:

				t = Test_RayCubeIntersect(gVector4(eyepx, eyepy, eyepz, 1), rayD, transformMat);
				point = transformMat.transpose().invert() * (gVector4(eyepx, eyepy, eyepz, 1) + rayD * t);
				
				for(int i = 0; i < 3; i++){
					if(point[i] < 0.49 && point[i] > -0.49 || foundEdge){
						point[i] = 0;
					}else{
						foundEdge = true;
					}
				}

				normal = point.getNormalized();
				break;

			case 3:

				gVector4 p1 = gVector4(0,1,0,1);
				gVector4 p2 = gVector4(1,-1,1,1);
				gVector4 p3 = gVector4(0,-1,-1,1);

				t = Test_RayPolyIntersect(gVector4(eyepx,eyepy,eyepz,1), rayD, p1, p2, p3, transformMat);

				normal = ((transformMat * p2-transformMat * p1) % (transformMat * p3-transformMat * p1)).getNormalized();
				break;
			}

			// normal = gVector4(eyepx,eyepy,eyepz,1) + rayD * t;

			float l = gVector4(5, 5, 5, 1).getNormalized() * normal.getNormalized(); //normalize it
			
			if(l < 0){
				l = 0;
			}
			//fout << t << '\t';
			
			//Set color according to t position
			if(t >= 0){
				int red = l * 255 + 30;
				int green = l * 255 + 30;
				int blue = l * 255 + 30;

				if(red > 255){
					red = 255;
				}
				if(green > 255){
					green = 255;
				}
				if(blue > 255){
					blue = 255;
				}
				
				output(x, y)->Red = red; //set color for when it's the material
				output(x, y)->Green = green;
				output(x, y)->Blue = blue;
			}else{
				output(x, y)->Red = 0; //set background color
				output(x, y)->Green = 0;
				output(x, y)->Blue = 0;
			}
		}
		//fout << '\n';
	}
	//fout.close();
	const char * conversion = name.c_str();
	output.WriteToFile(conversion);
}