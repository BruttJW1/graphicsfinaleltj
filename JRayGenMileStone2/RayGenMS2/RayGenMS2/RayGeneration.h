#pragma once

/*
  File:        RayGeneration.h for RayGenMS2
  Author:      Jonathan Brutt
  Class:	   Comp 361 A
  Purpose:     The purpose of this h is to provide a parse function to handle the text files.
 */

#include "EasyBMP.h"
#include <string>

class RayGeneration {

public:
	RayGeneration(); 
	~RayGeneration(void);
	static void parse(const std::string& filename); //Creating a ray for a bmp.
};