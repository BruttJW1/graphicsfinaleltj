/**
 File:        main.cpp for RayGenMS2
  Author:     Jonathan Brutt
  Class:	  Comp 361 A
  Purpose:     This program creates bmp files from text files for the purpose of ray generation using easyBMP
 */

#include "RayGeneration.h"
#include <string>
#include "EasyBMP.h"

using namespace std;

int main(int argc, char** argv) {

	cout << "Create BMPs" << endl;
	for(int i = 1; i <= 18; i++){
	//int i = 7;  //for debugging
		RayGeneration::parse(string("test")+ to_string(i) + string(".txt")); //for choosing the test files without massive copy pasting
		cout << "Finished with test" << i << ".txt\n\n" << endl;
	}
	return 0;
}